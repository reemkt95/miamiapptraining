export default (staticData = {
  ExploreMia: [
    {
      iconType: "MaterialCommunityIcons",
      iconName: "silverware-variant",
      cardTitle: "Dining",
      width: 200,
      height: 100,
      backgroundColor: "#0F4975"
    },
    {
      iconType: "MaterialIcons",
      iconName: "shopping-cart",
      cardTitle: "Shopping",
      width: 200,
      height: 100,
      backgroundColor: "#5E7AB2"
    },
    {
      iconType: "MaterialIcons",
      iconName: "airplanemode-active",
      cardTitle: "Airlines",
      width: 200,
      height: 100,
      backgroundColor: "#7BBCEF"
    },
    {
      iconType: "FontAwesome5",
      iconName: "parking",
      cardTitle: "Parking",
      width: 200,
      height: 100,
      backgroundColor: "#1F2768"
    },
    {
      iconType: "Ionicons",
      iconName: "ios-car",
      cardTitle: "Rental Cars",
      width: 200,
      height: 100,
      backgroundColor: "#0F4975"
    },
    {
      iconType: "MaterialIcons",
      iconName: "local-taxi",
      cardTitle: "Transportation",
      width: 200,
      height: 100,
      backgroundColor: "#5E7AB2"
    },
    {
      iconType: "AntDesign",
      iconName: "picture",
      cardTitle: "Art Exhibits",
      width: 200,
      height: 100,
      backgroundColor: "#7BBCEF"
    },
    {
      iconType: "MaterialIcons",
      iconName: "attach-money",
      cardTitle: "ATM & Banks",
      width: 200,
      height: 100,
      backgroundColor: "#1F2768"
    },
    {
      iconType: "MaterialCommunityIcons",
      iconName: "sofa",
      cardTitle: "VIP Clubs & Lounges",
      width: 200,
      height: 100,
      backgroundColor: "#0F4975"
    }
  ],

  connect: [
    {
      iconType: "AntDesign",
      iconName: "facebook-square",
      cardTitle: "iflyMIA",
      width: 200,
      height: 100,
      backgroundColor: "#245EA4"
    },
    {
      iconType: "AntDesign",
      iconName: "twitter",
      cardTitle: "iflyMIA",
      width: 200,
      height: 100,
      backgroundColor: "#245EA4"
    },
    {
      iconType: "AntDesign",
      iconName: "twitter",
      cardTitle: "iflyMIA",
      width: 200,
      height: 100,
      backgroundColor: "#65B6FA"
    },
    {
      iconType: "Foundation",
      iconName: "social-instagram",
      cardTitle: "iflyMIA",
      width: 200,
      height: 100,
      backgroundColor: "#196A8F"
    },
    {
      iconType: "Foundation",
      iconName: "social-instagram",
      cardTitle: "www.miami-airpoty.com",
      width: 200,
      height: 100,
      backgroundColor: "#196A8F"
    }
  ],

  food: [
    {
      source: require("../resources/food/restaurant.jpg"),
      imageTitle: "restaurant"
    },
    {
      source: require("../resources/food/bar.jpeg"),
      imageTitle: "Bar"
    },
    {
      source: require("../resources/food/coffee.jpg"),
      imageTitle: "Coffee"
    },
    {
      source: require("../resources/food/breakfast.jpeg"),
      imageTitle: "Breakfast"
    },
    {
      source: require("../resources/food/snacks.jpg"),
      imageTitle: "Snacks"
    },
    {
      source: require("../resources/food/burger.jpg"),
      imageTitle: "Burgers"
    },
    {
      source: require("../resources/food/sandwiches.jpg"),
      imageTitle: "Sandwiches"
    },
    {
      source: require("../resources/food/pizza.jpg"),
      imageTitle: "Pizza"
    },
    {
      source: require("../resources/food/salads.jpg"),
      imageTitle: "Salads"
    },
    {
      source: require("../resources/food/pastry.jpg"),
      imageTitle: "Pastry"
    }
  ],
  shopping: [
    {
      source: require("../resources/shopping/bag.jpg"),
      imageTitle: "Accessories"
    },
    {
      source: require("../resources/shopping/phone.jpg"),
      imageTitle: "Phone Accessories"
    },
    {
      source: require("../resources/shopping/travel.jpg"),
      imageTitle: "Travel Essentials"
    },
    {
      source: require("../resources/shopping/apaprel.jpg"),
      imageTitle: "Apparel"
    },
    {
      source: require("../resources/shopping/reading.jpg"),
      imageTitle: "Reading Materials"
    },
    {
      source: require("../resources/shopping/fragrances.jpg"),
      imageTitle: "Fragrances"
    },
    {
      source: require("../resources/shopping/jewelry.jpg"),
      imageTitle: "Jewelery"
    },
    {
      source: require("../resources/shopping/pharmacy.jpg"),
      imageTitle: "Pharmacy"
    }
  ],
  relax: [
    {
      source: require("../resources/relax/lounge.jpg"),
      imageTitle: "Lounge"
    },
    {
      source: require("../resources/relax/manicure.jpg"),
      imageTitle: "Manicure"
    },
    {
      source: require("../resources/relax/massage.jpg"),
      imageTitle: "Massage"
    },
    {
      source: require("../resources/relax/spa.jpg"),
      imageTitle: "Spa"
    }
  ],
  consourses: [
    {
      title: "Concourse D",
      width: 200,
      height: 100,
      backgroundColor: "#5E7AB2"
    },
    {
      title: "Concourse E",
      width: 200,
      height: 100,
      backgroundColor: "#7BBCEF"
    },
    {
      title: "Concourse F",
      width: 200,
      height: 100,
      backgroundColor: "#1F2768"
    },
    {
      title: "Concourse G",
      width: 200,
      height: 100,
      backgroundColor: "#0F4975"
    },
    {
      title: "Concourse H",
      width: 200,
      height: 100,
      backgroundColor: "#5E7AB2"
    },
    {
      title: "Concourse J",
      width: 200,
      height: 100,
      backgroundColor: "#7BBCEF"
    }
  ],
  ExploreToolbar: {
    top: [
      {
        iconName: "user-circle",
        iconType: "FontAwesome"
      },
      {
        tag: "Explore"
      },
      {
        iconName: "md-search",
        iconType: "Ionicons"
      },
      {
        iconName: "message-text",
        iconType: "MaterialCommunityIcons"
      }
    ],
    bottom: [
      {
        iconType: "Ionicons",
        iconName: "md-home"
      },
      {
        iconName: "airplanemode-active",
        iconType: "MaterialIcons"
      },
      {
        iconName: "md-compass",
        iconType: "Ionicons"
      },
      {
        iconName: "md-map",
        iconType: "Ionicons"
      },
      {
        iconName: "ios-information-circle",
        iconType: "Ionicons"
      }
    ]
  },
  Flight: [
    {
      iconType: "MaterialCommunityIcons",
      iconName: "airplane-landing",
      cardTitle: "Arrival",
      width: 170,
      height: 110,
      backgroundColor: "#0F4975"
    },
    {
      iconType: "MaterialCommunityIcons",
      iconName: "airplane-takeoff",
      cardTitle: "Departures",
      width: 170,
      height: 110,
      backgroundColor: "#0F4975"
    },
    {
      iconType: "Ionicons",
      iconName: "md-barcode",
      cardTitle: "Scan Boarding Pass",
      width: 170,
      height: 110,
      backgroundColor: "#0F4975"
    },
    {
      iconType: "Ionicons",
      iconName: "md-airplane",
      cardTitle: "Airlines",
      width: 170,
      height: 110,
      backgroundColor: "#0F4975"
    }
  ],
  Dining: [
    { key: "American" },
    { key: "Appetizers" },
    { key: "Asian" },
    { key: "Bagel" },
    { key: "Bakery" },
    { key: "Bar" },
    { key: "Beer" },
    { key: "Breakfast" },
    { key: "burger" },
    { key: "Cafe" },
    { key: "Caribbean" },
    { key: "Chinese" },
    { key: "Coffee" },
    { key: "Cuban" },
    { key: "Dessert" },
    { key: "Donut" },
    { key: "Fast food" },
    { key: "Fish" },
    { key: "Fries" },
    { key: "Hot dog" },
    { key: "Ice cream" },
    { key: "Italian" },
    { key: "Margarita" },
    { key: "Mexican" },
    { key: "Noodles" },
    { key: "Panini" },
    { key: "Pastry" },
    { key: "Pizza" },
    { key: "Pub fare" },
    { key: "Restaurant" },
    { key: "Salads" },
    { key: "Sandwiches" },
    { key: "Sashimi" },
    { key: "Seafood" },
    { key: "Shrimp" },
    { key: "Smoothie" },
    { key: "Snacks" },
    { key: "Soup" },
    { key: "Steak" },
    { key: "Sushi" },
    { key: "To go" }
  ],
  foodTypes: {
    American: [
      {
        name: "Budweiser Brew House",
        location: "Concourse F - Gate F12"
      },
      {
        name: "Budweiser Brew House",
        location: "Concourse H - Gate H12"
      },
      {
        name: "Chef Creole",
        location: "Central Terminal - Check-in"
      }
    ],
    Appetizers: [
      {
        name: "Chilli's Too",
        location: "Concourse F - Gate F12"
      },
      {
        name: "Corona Beach House",
        location: "Concourse H - Gate H12"
      },
      {
        name: "Clubhouse One",
        location: "Central Terminal - Check-in"
      }
    ]
  },
  DinerDetails: {
    Budweiser_Brew_House: [
      {
        title: "Budweiser Brew House",
        info:
          "Grab an ice-cold Budweiser on draft, or any other favorite from the full bar. Great appertizers, too."
      },
      {
        title: "Mon thru Sun",
        info: "10:00 AM - 9:00 PM"
      },
      {
        info: "+1 (305) 492-1723"
      },
      {
        info: "Concourse F - Gate F12"
      }
    ]
  }
});
