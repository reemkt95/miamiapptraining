import React, { Component } from "react";
import { View, Image, Text, StyleSheet } from "react-native";
import { Icon, Right } from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import staticInfo from "../staticData/staticInfo";
export default class ItemDetail extends Component {
  render() {
    const { navigate } = this.props.navigate;
    let details;
    if (this.props.details) {
      details = <Text>{this.props.details}</Text>;
    }
    return (
      <View
        style={{
          flexDirection: "row",
          borderBottomWidth: 1,
          borderColor: "#A9ABAF",
          padding: 4
        }}
      >
        <Image
          style={{
            marginTop: 3,
            backgroundColor: "#000000",
            width: this.props.dims,
            height: this.props.dims,
            borderRadius: 40
          }}
          source={this.props.source}
        />

        <View
          style={{
            flex: 1,
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            marginLeft: 10
          }}
        >
          <View
            style={{
              display: "flex",
              flex: 1,
              flexDirection: "column"
            }}
          >
            <Text style={styles.titleStyle}>{this.props.title}</Text>

            {details}
          </View>

          <Icon
            style={{
              position: "absolute",
              right: 0,
              fontSize: 12,
              color: "#A9ABAF"
            }}
            type="FontAwesome"
            name="chevron-right"
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 16,
    color: "black"
  }
});
