import React, { Component } from "react";
import { Card, CardItem, Icon } from "native-base";
import {
  StyleSheet,
  Image,
  Text,
  View,
  ScrollView,
  ToastAndroid
} from "react-native";
import { Container, Header, Toast } from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";

import ImageCard from "./ImageCard";

export default class HeaderBottom extends Component {
  render() {
    let icons = this.props.itemsArray.map(x => {
      return (
        <Col style={{ alignItems: "center" }}>
          <Icon
            name={x.iconName}
            type={x.iconType}
            style={{
              fontSize: 22,
              color: "#FFFFFF"
            }}
            onPress={() => {
              ToastAndroid.show("I don't navigate yet", ToastAndroid.LONG);
            }}
          />
        </Col>
      );
    });
    return (
      <View>
        <Grid>
          <Row>{icons}</Row>
        </Grid>
      </View>
    );
  }
}
