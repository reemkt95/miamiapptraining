import React, { Component } from "react";
import { Card, CardItem, Icon } from "native-base";
import {
  StyleSheet,
  Image,
  Text,
  View,
  Animated,
  ImageBackground,
  ScrollView,
  ToastAndroid
} from "react-native";
import staticInfo from "../staticData/staticInfo";
import { Container, Header, Toast } from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";

import ImageCard from "./ImageCard";
import HeaderTop from "./headerTop";
import HeaderBottom from "./headerBottom";

export default class HeaderComponent extends Component {
  render() {
    return (
      <Animated.View
        style={{
          justifyContent: "center",
          alignItems: "center",
          position: "absolute",
          left: 0,
          right: 0,
          height: this.props.AnimateHeaderHeight,
          color: this.props.AnimateHeaderBackgroundColor
        }}
      >
        <ImageBackground
          source={this.props.source}
          imageStyle={{ opacity: 0.5 }}
          style={{
            backgroundColor: "blue",
            width: "100%",
            height: "100%"
          }}
        >
          <HeaderTop itemsArray={staticInfo.ExploreToolbar.top} />
          <Animated.View
            style={{ marginTop: this.props.AnimatedToolbarMargin }}
          />
        </ImageBackground>
      </Animated.View>
    );
  }
}
