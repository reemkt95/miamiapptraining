import React, { Component } from "react";
import { Card, CardItem, Icon } from "native-base";
import {
  StyleSheet,
  Image,
  Text,
  View,
  ScrollView,
  ToastAndroid
} from "react-native";
import { Container, Header, Left, Right, Body } from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";

import ImageCard from "./ImageCard";

export default class HeaderTop extends Component {
  render() {
    let icons = this.props.itemsArray.map((x, index) => {
      if (x.tag) {
        return (
          <View style={{ alignItems: "center" }}>
            <Text style={{ fontSize: 18, color: "#FFFFFF" }}>{x.tag}</Text>
          </View>
        );
      }
      return (
        <Icon
          name={x.iconName}
          type={x.iconType}
          style={{
            fontSize: 16,
            color: "#FFFFFF"
          }}
          onPress={() => {
            ToastAndroid.show("I don't navigate yet", ToastAndroid.LONG);
          }}
        />
      );
    });
    return (
      <View style={{ margin: 20 }}>
        <Row>
          <Col style={{ alignItems: "flex-start" }}>{icons[0]}</Col>

          <Col style={{ alignItems: "center" }}>{icons[1]}</Col>

          <Col style={{ alignItems: "flex-end" }}>
            <View style={{ marginRight: 20 }}>{icons[2]}</View>
            <View>{icons[3]}</View>
          </Col>
        </Row>
        {/* <Header transparent>
          <Left>{icons[0]}</Left>
          <Body style={{ alignItems: "flex-end" }}>{icons[1]}</Body>
          <Right>
            <View style={{ marginRight: 20 }}>{icons[2]}</View>
            <View>{icons[3]}</View>
          </Right>
        </Header> */}
      </View>
    );
  }
}

{
  /* <Col>
              <Icon
                name="bars"
                type="FontAwesome"
                style={{ fontSize: 16, color: "#FFFFFF" }}
                onPress={() => {
                  ToastAndroid.show("I don't navigate yet", ToastAndroid.LONG);
                }}
              />
            </Col>
            <Col>
              <Text style={{ fontSize: 16, color: "#FFFFFF" }}>Explore</Text>
            </Col>
            <Col>
              <Icon
                name="md-search"
                style={{ fontSize: 16, color: "#FFFFFF" }}
                onPress={() => {
                  ToastAndroid.show("I don't navigate yet", ToastAndroid.LONG);
                }}
              />
            </Col>
            <Col>
              <Icon
                type="MaterialCommunityIcons"
                name="message-text"
                style={{ fontSize: 16, color: "#FFFFFF" }}
                onPress={() => {
                  ToastAndroid.show("I don't navigate yet", ToastAndroid.LONG);
                }}
              />
            </Col> */
}
