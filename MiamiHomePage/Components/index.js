import AirportInfo from "./AirportInfo";

import DiningPage from "./DiningPage";
import Flights from "./FlightsPage";
import HeaderComponent from "./Header";
import HeaderBottom from "./headerBottom";
import DiningDetails from "./DiningDetails";
import HeaderTop from "./headerTop";
import HomePage from "./HomePage";
import IconCard from "./IconCard";
import DinerDescription from "./DinerDescription";
import ImageCard from "./ImageCard";
import ExplorePage from "./ExplorePage";
import Map from "./Map";

export default (components = {
  AirportInfo: AirportInfo,

  DiningPage: DiningPage,
  DiningDetails: DiningDetails,
  DinerDescription: DinerDescription,
  Flights: Flights,
  HeaderComponent: HeaderComponent,
  HeaderBottom: HeaderBottom,
  HeaderTop: HeaderTop,
  HomePage: HomePage,
  IconCard: IconCard,
  ImageCard: ImageCard,
  ExplorePage: ExplorePage,
  Map: Map
});
