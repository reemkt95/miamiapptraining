import React, { Component } from "react";
import { Card, CardItem, Icon } from "native-base";
import { StyleSheet, Image, Text, View, ScrollView } from "react-native";
import { Container, Header } from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";

export default class ImageCard extends Component {
  render() {
    let cards = this.props.itemsArray.map(x => {
      return (
        <View
          style={{
            width: 100,
            marginLeft: 10,
            marginTop: 15,
            alignItems: "center"
          }}
        >
          <Image
            style={{ borderRadius: 5, height: 100, width: 100 }}
            source={x.source}
          />
          <Text style={{ color: "#000000" }}>{x.imageTitle}</Text>
        </View>
      );
    });
    return (
      <View
        style={{
          borderBottomColor: "#B5AEAE",
          borderBottomWidth: this.props.hasBorder ? 1 : 0,
          height: 180
        }}
      >
        <Row>
          <Col>
            <Text
              style={{
                fontSize: 16,
                color: "#000000",
                marginTop: 5,
                marginLeft: 7
              }}
            >
              {this.props.title}
            </Text>
          </Col>
          {this.props.hasSeeAll ? (
            <Col style={{ alignItems: "flex-end" }}>
              <Text
                style={{
                  color: "grey",
                  fontWeight: "bold",
                  marginRight: 10,
                  marginTop: 10,
                  padding: 5
                }}
              >
                See ALL ->
              </Text>
            </Col>
          ) : (
            <Text />
          )}
        </Row>
        <ScrollView horizontal={true} style={{ height: 120 }}>
          {cards}
        </ScrollView>
      </View>
    );
  }
}
