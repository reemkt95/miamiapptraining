import React, { Component } from "react";
import { Text, View, ScrollView, StyleSheet } from "react-native";

import { Card, CardItem, Icon } from "native-base";

export default class IconCard extends Component {
  render() {
    let cards = this.props.itemsArray.map(x => {
      if (x.iconType)
        return (
          <View
            style={{
              width: x.width,
              height: x.height,
              backgroundColor: x.backgroundColor,
              margin: 5,
              borderRadius: 5
            }}
          >
            <Icon style={styles.cardIcon} name={x.iconName} type={x.iconType} />
            <Text style={styles.cardText}>{x.cardTitle}</Text>
          </View>
        );
      else
        return (
          <View
            style={{
              width: x.width,
              height: x.height,
              backgroundColor: x.backgroundColor,
              margin: 5,
              paddingTop: 40,
              borderRadius: 5
            }}
          >
            <Text style={styles.cardText}>{x.title}</Text>
          </View>
        );
    });

    let text;
    if (this.props.title) {
      text = <Text style={styles.viewTitle}>{this.props.title}</Text>;
    }
    return (
      <View
        style={{
          height: this.props.height,
          marginTop: 5
        }}
      >
        {text}
        <ScrollView
          scrollEnabled={this.props.scrollable}
          horizontal={true}
          style={{
            borderBottomColor: this.props.hasBorder ? "#B5AEAE" : "",
            borderBottomWidth: this.props.hasBorder ? 1 : 0
          }}
        >
          {cards}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardText: {
    textAlign: "center",
    color: "white",
    fontSize: 16,
    flex: 1.5
  },
  cardIcon: {
    color: "white",
    fontSize: 40,
    textAlign: "center",
    flex: 2
  },
  viewTitle: {
    fontSize: 16,
    color: "#000000",
    marginTop: 10,
    marginLeft: 15
  }
});
