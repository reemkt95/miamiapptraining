import React from "react";
import { Platform, Dimensions } from "react-native";
import { createDrawerNavigator, createAppContainer } from "react-navigation";

import SeeAllPage from "../Pages/SeeAllPage";

import TabNavigator from "./TabNavigator";
import StackNavigator from "./StackNavigator";

const WIDTH = Dimensions.get("window").width; // gets the width of the window

const DrawerConfig = {
  drawerWidth: WIDTH * 0.83
};
const DrawerNavigator = createDrawerNavigator(
  {
    Tabs: { screen: TabNavigator },
    Dining: {
      screen: StackNavigator
    }
  },

  DrawerConfig
);

export default createAppContainer(DrawerNavigator);
