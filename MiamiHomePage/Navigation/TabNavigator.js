import HomePage from "../Pages/HomePage"
import FlightsPage from "../Pages/FlightsPage"
import ExplorePage from "../Pages/ExplorePage"
import MapPage from "../Pages/MapPage"
import AirlineInfoPage from "../Pages/AirlineInfoPage"

import React from "react";
import {
  createBottomTabNavigator,
  createMaterialTopTabNavigator,
  createAppContainer
} from "react-navigation";

const TabNavigator = createBottomTabNavigator({
  Home: HomePage ,
  Flights: FlightsPage,
  Explore: ExplorePage,
  Map: MapPage,
  AirlineInfoPage: AirlineInfoPage
});

export default createAppContainer(TabNavigator);
