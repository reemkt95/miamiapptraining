import React, { Component } from "react";
import OptionsListPage from "../Pages/OptionsListPage";
import SeeAllPage from "../Pages/SeeAllPage";
import DetailsPage from "../Pages/DetailsPage";

import { createAppContainer, createStackNavigator } from "react-navigation";

const CreateStackNavigator = createStackNavigator({
  SeeAllPage: {
    screen: SeeAllPage
  },

  OptionsListPage: OptionsListPage,
  DetailsPage: DetailsPage
});

const AppContainer = createAppContainer(CreateStackNavigator);

export default AppContainer;
