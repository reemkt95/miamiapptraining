/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */


import DrawerNavigator from "./Navigation/DrawerNavigator";
import React, { Component } from "react";


export default class App extends Component {
  render() {
    return <DrawerNavigator />;
  }
}
