import React, { Component } from "react";
import { View, ScrollView, FlatList, StyleSheet } from "react-native";

import { Col, Row, Grid } from "react-native-easy-grid";
import ItemDetail from "../Components/itemDetail";
export default class OptionsListPage extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam("title")
    };
  };

  render() {
    return (
      <ScrollView style={styles.mainView}>
        <Grid>
          <Col>
            <FlatList
              data={this.props.navigation.getParam("data", {})}
              renderItem={({ item }) => (
                <ItemDetail
                  dims={60}
                  navigate={this.props.navigation}
                  source={require("../resources/cup.jpg")}
                  details={item.location}
                  title={item.name}
                />
              )}
            />
          </Col>
        </Grid>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  mainView: {
    marginTop: 20
  }
});
