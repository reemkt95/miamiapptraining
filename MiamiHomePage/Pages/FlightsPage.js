import React, { Component } from "react";
import { View, ScrollView, Image, Text, Animated } from "react-native";

import { Col, Row, Grid } from "react-native-easy-grid";
import IconCard from "../Components/IconCard";
import staticData from "../staticData/staticInfo";
import HeaderComponent from "../Components/Header";

const maxHeight = 250;
const minHeight = 100;
export default class Flights extends Component {
  constructor(props) {
    super(props);
    this.AnimatedHeaderValue = new Animated.Value(0);
    this.state = {};
  }

  componentAnimation(inputRange, outputRange) {
    return (AnimateHeaderBackgroundColor = this.AnimatedHeaderValue.interpolate(
      {
        inputRange: inputRange,

        outputRange: outputRange,

        extrapolate: "clamp"
      }
    ));
  }

  render() {
    return (
      <View>
        <ScrollView
          scrollEventThrottle={16}
          contentContainerStyle={{ paddingTop: maxHeight }}
          onScroll={Animated.event([
            {
              nativeEvent: { contentOffset: { y: this.AnimatedHeaderValue } }
            }
          ])}
        >
          <Grid style={{ alignItems: "center" }}>
            <Col>
              <View style={{ alignItems: "center", marginTop: 10 }}>
                <Text style={{ color: "black", fontSize: 22 }}>
                  Flight Information
                </Text>
                <Text style={{ fontSize: 14, color: "#C9C9C9" }}>
                  Select a flight and we'll keep you posted.
                </Text>
              </View>
            </Col>
            <Row>
              <IconCard
                height={115}
                hasBorder={false}
                scrollable={false}
                itemsArray={staticData.Flight.slice(0, 2)}
              />
            </Row>

            <Row>
              <IconCard
                height={115}
                hasBorder={true}
                scrollable={false}
                itemsArray={staticData.Flight.slice(2)}
              />
            </Row>
            <Col style={{ height: 500, alignItems: "center" }}>
              <Image
                source={require("../resources/miami-hotel.jpg")}
                style={{ marginTop: 40, height: 300, width: 300 }}
              />
            </Col>
          </Grid>
        </ScrollView>
        <HeaderComponent
          source={require("C:/Users/User/miamiapptraining/MiamiHomePage/resources/header/flights.jpg")}
          AnimatedToolbarMargin={this.componentAnimation([0, 100], [210, 60])}
          AnimateHeaderBackgroundColor={this.componentAnimation(
            [0, maxHeight - minHeight],
            ["#ffffff", "#0A36B0"]
          )}
          AnimateHeaderHeight={this.componentAnimation(
            [0, maxHeight - minHeight],
            [maxHeight, minHeight]
          )}
        />
      </View>
    );
  }
}
