import React, { Component } from "react";
import { View, ScrollView, Image, Animated } from "react-native";
import { Row, Grid, Col } from "react-native-easy-grid";
import IconCard from "../Components/IconCard";
import ImageCard from "../Components/ImageCard";
import staticData from "../staticData/staticInfo";
import HeaderComponent from "../Components/Header";

const maxHeight = 250;
const minHeight = 100;
export default class ExplorePage extends Component {
  constructor(props) {
    super(props);
    this.AnimatedHeaderValue = new Animated.Value(0);
  }

  componentAnimation(inputRange, outputRange) {
    return (AnimateHeaderBackgroundRowor = this.AnimatedHeaderValue.interpolate(
      {
        inputRange: inputRange,

        outputRange: outputRange,

        extrapolate: "clamp"
      }
    ));
  }

  render() {
    return (
      <View>
        <ScrollView
          scrollEventThrottle={16}
          contentContainerStyle={{ paddingTop: maxHeight }}
          onScroll={Animated.event([
            {
              nativeEvent: { contentOffset: { y: this.AnimatedHeaderValue } }
            }
          ])}
        >
          <Grid>
            <Row>
              <IconCard
                height={150}
                hasBorder={true}
                itemsArray={staticData.ExploreMia}
                title="Explore MIA"
              />
            </Row>
            <Row>
              <ImageCard
                hasSeeAll={true}
                itemsArray={staticData.food}
                title="I want..."
                hasBorder={true}
              />
            </Row>
            <Row>
              <ImageCard
                hasSeeAll={true}
                hasBorder={true}
                itemsArray={staticData.shopping}
                title="I want to shop for..."
              />
            </Row>
            <Row>
              <ImageCard
                hasSeeAll={true}
                hasBorder={true}
                itemsArray={staticData.relax}
                title="I want to relax"
              />
            </Row>
            <Row>
              <IconCard
                height={150}
                hasBorder={true}
                itemsArray={staticData.consourses}
                title="Guide to Concourses"
              />
            </Row>
            <Row>
              <IconCard
                height={150}
                hasBorder={true}
                itemsArray={staticData.connect}
                title="Connect with us"
              />
            </Row>
            <Col style={{ height: 500, alignItems: "center" }}>
              <Image
                source={require("../resources/miami-hotel.jpg")}
                style={{ marginTop: 40, height: 300, width: 300 }}
              />
            </Col>
          </Grid>
        </ScrollView>
        <HeaderComponent
          source={require("C:/Users/User/miamiapptraining/MiamiHomePage/resources/header/headerBackgound.jpg")}
          AnimatedToolbarMargin={this.componentAnimation([0, 100], [210, 60])}
          AnimateHeaderBackgroundRowor={this.componentAnimation(
            [0, maxHeight - minHeight],
            ["#ffffff", "#0A36B0"]
          )}
          AnimateHeaderHeight={this.componentAnimation(
            [0, maxHeight - minHeight],
            [maxHeight, minHeight]
          )}
        />
      </View>
    );
  }
}
