import React, { Component } from "react";
import {
  View,
  ScrollView,
  ImageBackground,
  Text,
  FlatList,
  StyleSheet
} from "react-native";

import { Col, Row, Grid } from "react-native-easy-grid";

import staticInfo from "../staticData/staticInfo";

export default class DetailsPage extends Component {
  static navigationOptions = {
    headerTransparent: true,
    headerTintColor: "#FFFFFF"
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View>
        <ImageBackground
          style={{
            alignItems: "center",
            width: "100%",
            height: 250
          }}
          source={require("../resources/header/diner.jpg")}
        />
        <ScrollView>
          <FlatList
            data={this.props.navigation.getParam("data", {})}
            renderItem={({ item }) => (
              <View style={styles.mainView}>
                <Text
                  style={{
                    fontSize: item.title ? 16 : 0,
                    color: "navy",
                    marginTop: item.title ? 10 : 0
                  }}
                >
                  {item.title}
                </Text>
                <Text
                  style={{
                    margin: item.title ? 0 : "3%",
                    fontSize: 16,
                    color: "black"
                  }}
                >
                  {item.info}
                </Text>
              </View>
            )}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    alignItems: "center",
    width: "100%",
    height: 250
  },
  mainView: {
    borderBottomWidth: 1,
    marginLeft: 25,
    marginRight: 25
  }
});
