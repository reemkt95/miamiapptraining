import React, { Component } from "react";
import { View, ScrollView, Text, FlatList, StyleSheet } from "react-native";

import { Col, Row, Grid } from "react-native-easy-grid";
import { Card, CardItem, Icon } from "native-base";
import ImageCard from "../Components/ImageCard";

import staticInfo from "../staticData/staticInfo";

export default class SeeAllPage extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  static navigationOptions = {
    title: "Dining"
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
        <Grid>
          <Col>
            <ImageCard
              itemsArray={staticInfo.food}
              title="Popular"
              hasBorder={false}
              hasSeeAll={false}
            />
          </Col>
          <Col>
            <Text style={styles.label}>Others</Text>
          </Col>

          <FlatList
            data={staticInfo.Dining}
            renderItem={({ item }) => (
              <Grid
                onPress={() => {
                  let key = item.key;
                  navigate("OptionsListPage", {
                    data: staticInfo.foodTypes[key],
                    title: key
                  });
                }}
                style={styles.rowStyle}
              >
                <Row>
                  <Col style={{ alignItems: "flex-start" }}>
                    <Text style={styles.text}>{item.key}</Text>
                  </Col>
                  <Col style={{ alignItems: "flex-end" }}>
                    <Icon
                      name="chevron-right"
                      type="MaterialCommunityIcons"
                      style={[
                        styles.text,
                        {
                          marginRight: 15
                        }
                      ]}
                    />
                  </Col>
                </Row>
              </Grid>
            )}
          />

          <Row />
        </Grid>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  label: {
    fontSize: 16,
    color: "#000000",
    marginTop: 5,
    marginLeft: 7
  },
  rowStyle: {
    borderBottomWidth: 1,
    borderBottomColor: "#B5AEAE",
    marginLeft: 10,
    height: 40
  },
  text: {
    fontSize: 16,
    color: "#000000",
    marginTop: 10
  }
});
